{{define "menu"}}

<div class="ui secondary pointing menu">
  <a id="home" class="item" href="/rasta/">
    Home
  </a>
  <a class="item">
    Time Report
  </a>
  <a id="users" class="item" href="/rasta/users/">
    Users
  </a>
  <div class="right menu">
    <a class="ui item">
      Log Out
    </a>
  </div>
</div>

{{end}}

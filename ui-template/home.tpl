{{define "home"}}
<!DOCTYPE html>
<html>
<head>
  {{template "header"}}
  <title>Rasta Time Clock</title>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>

</head>
<body>

<div class="ui container">

{{template "menu"}}

<div class="ui segment">
  <h2 class="ui header">Welcome {{.Firstname}} {{.Lastname}}</h2>
  <p>You are currently {{if .ClockedIn}}{{else}}not {{end}}clocked in.
     {{if .ClockedIn}}
     <button id="clockout" class="ui labeled icon button">
       <i class="sign out icon"></i>Clock Out
     </button>
     {{else}}
     <button id="clockin" class="ui labeled icon button">
       <i class="sign in icon"></i>Clock In
     </button>
     {{end}}
  </p>

  <div class="ui feed">
    {{range $index, $elem := .Records}}
    {{if $elem.TimedOut}}
    <div class="event">
      <div class="label">
        <i class="sign out icon"></i>
      </div>
      <div class="content">
        <div class="date">
          {{ftime $elem.TimeOut}}
        </div>
        <div class="summary">
          You clocked out
        </div>
      </div>
    </div>
    {{end}}

    <div class="event">
      <div class="label">
        <i class="sign in icon"></i>
      </div>
      <div class="content">
        <div class="date">
          {{ftime $elem.TimeIn}}
        </div>
        <div class="summary">
          You clocked in
        </div>
      </div>
    </div>
    {{end}}
  </div>
</div>

</div>

<script>
   $('#home').addClass("active");
   $('#clockin').api({
     action: 'clock in out',
     method: 'POST',
     onSuccess: function(response) {
       location.reload(true);
     },
     onError: function(response) {
       console.log("Failed to Clock in");
     }
   });
   $('#clockout').api({
     action: 'clock in out',
     method: 'PATCH',
     onSuccess: function(response) {
       location.reload(true);
     },
     onError: function(response) {
       console.log("Failed to Clock out");
     }
   });
</script>

</body>

</html>
{{end}}

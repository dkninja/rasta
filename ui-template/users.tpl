{{define "users"}}
<!DOCTYPE html>
<html>
<head>
  {{template "header"}}
  <title>Rasta Time Clock</title>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
    .ui.grid > .hidden {
      display: none;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            username: {
              identifier  : 'username',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter a username'
                },
                {
                  type   : 'length[4]',
                  prompt : 'Username must be at least 4 characters'
                },
                {
                  type   : 'maxLength[64]',
                  prompt : 'Username must be at most 64 characters'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter a password'
                },
                {
                  type   : 'length[4]',
                  prompt : 'Password must be at least 4 characters'
                },
                {
                  type   : 'maxLength[64]',
                  prompt : 'Password must be at most 64 characters'
                }
              ]
            },
            firstname: {
              identifier  : 'firstname',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter user\'s first name'
                },
                {
                  type   : 'length[4]',
                  prompt : 'First name must be at least 4 characters'
                },
                {
                  type   : 'maxLength[64]',
                  prompt : 'First name must be at most 64 characters'
                }
              ]
            },
            lastname: {
              identifier  : 'lastname',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter user\'s last name'
                },
                {
                  type   : 'length[4]',
                  prompt : 'Last name must be at least 4 characters'
                },
                {
                  type   : 'maxLength[64]',
                  prompt : 'Last name must be at most 64 characters'
                }
              ]
            },
          }
        })
      ;
    })
  ;
  </script>

</head>
<body>

<div class="ui container">

{{template "menu"}}

{{if .IsAdmin}}
<div class="ui middle aligned center aligned grid">
  <div class="row"><div class="column">
    <div class="ui segment">
      <button id="adduser" class="ui labeled icon button">
        <i class="add user icon"></i>
        Add User
      </button>
      <button class="ui labeled icon button">
        <i class="pencil icon"></i>
        Edit User
      </button>
    </div>
  </div></div>
  <div id="adduserrow" class="row hidden"><div class="column">
    <form class="ui large form" id="adduser">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left input">
            <input type="text" name="user[firstname]" placeholder="First Name">
          </div>
        </div>
        <div class="field">
          <div class="ui left input">
            <input type="text" name="user[lastname]" placeholder="Last Name">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="user[username]" placeholder="Username">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="user[password]" placeholder="Password">
          </div>
        </div>
        <div class="field">
          <div class="ui toggle checkbox">
            <input name="user[admin]" type="checkbox">
            <label>Administrator</label>
          </div>
        </div>
        <div class="ui fluid large submit button">Add</div>
      </div>

      <div class="ui error message"></div>

    </form>
  </div></div>
</div>

{{else}}
<h2 class="ui header">Not Authorized</h2>
{{end}}

</div>

</body>

<script>
   $('#users').addClass("active");
   $('button#adduser').on("click", function() {
     if ( $('#adduserrow').hasClass("hidden") ) {
       $('#adduserrow').removeClass("hidden");
     } else {
       $('#adduserrow').addClass("hidden");
     }
   });
   $('form#adduser .submit.button').api({
     action: 'create user',
     method: 'POST',
     serializeForm: true,
     onSuccess: function(response) {
       location.reload(true);
     }
   });
</script>

</html>
{{end}}

{{define "header"}}

  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/reset.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/site.css">

  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/container.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/grid.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/header.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/image.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/menu.css">

  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/divider.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/segment.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/form.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/input.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/button.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/list.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/checkbox.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/message.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/icon.css">
  <link rel="stylesheet" type="text/css" href="/assets/library/semantic-ui/components/feed.css">

  <script src="/assets/library/jquery.min.js"></script>
  <script src="/assets/library/jquery.serialize-object.min.js"></script>
  <script src="/assets/library/semantic-ui/components/api.js"></script>
  <script src="/assets/library/settings.js"></script>
  <script src="/assets/library/semantic-ui/components/form.js"></script>
  <script src="/assets/library/semantic-ui/components/transition.js"></script>
{{end}}

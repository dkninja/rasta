{{define "login"}}
<!DOCTYPE html>
<html>
<head>
  {{template "header"}}
  <title>Login to Rasta</title>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            username: {
              identifier  : 'username',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your username'
                },
                {
                  type   : 'length[4]',
                  prompt : 'Your username must be at least 4 characters'
                },
                {
                  type   : 'maxLength[64]',
                  prompt : 'Your username must be at most 64 characters'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                },
                {
                  type   : 'length[4]',
                  prompt : 'Your password must be at least 4 characters'
                },
                {
                  type   : 'maxLength[64]',
                  prompt : 'Your password must be at most 64 characters'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <img src="/assets/images/logo.png" class="image">
      <div class="content">
        Log-in to rasta
      </div>
    </h2>
    <form class="ui large form{{if .PassErr}} error{{end}}" action="/rasta/login/" method="post">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="username" placeholder="Username" value="{{.Username}}">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Login</div>
      </div>

      <div class="ui error message">{{if .PassErr}}<ul class="list"><li>Incorrect username or password</li></ul>{{end}}</div>

    </form>
  </div>
</div>

</body>

</html>
{{end}}

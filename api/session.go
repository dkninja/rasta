package api

import (
	"net/http"
	"encoding/json"
	"io"
	"errors"
	"bitbucket.org/dkninja/rasta/sessions"
	"bitbucket.org/dkninja/rasta/db"
	"bitbucket.org/dkninja/rasta/schema/users"
)

type SessionResponseData struct {
	SessionId string `json:"session_id"`
}

type SessionRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func UserLoggedIn(r *http.Request) bool {
	c, err := r.Cookie("session_id")
	if err != nil {
		// error fetching cookie
		return false
	}
	// cookie found
	// lookup session id in active map
	sessions.GetSessionManager().SessionLock.Lock()
	defer sessions.GetSessionManager().SessionLock.Unlock()
	session := sessions.GetSessionManager().Sessions[c.Value]
	if session != nil && !session.IsExpired() {
		return true
	}
	return false
}

func session_api_handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		dec := json.NewDecoder(r.Body)
		var s SessionRequest
		if err := dec.Decode(&s); err == io.EOF {
			Error(http.StatusBadRequest, w, errors.New("Ill formed Request"))
		} else if err != nil {
			Error(http.StatusBadRequest, w, errors.New("Ill formed Request"))
		} else {
			if dec.More() {
				Error(http.StatusBadRequest, w, errors.New("Ill formed Request"))
			} else if len(s.Username) < 4 || len(s.Password) < 4 {
				Error(http.StatusUnprocessableEntity, w, errors.New("Parameters too short"))
			} else {
				// else go and check the db
				dbh, err := db.OpenHelper("sqlite3", "./rasta.db?_busy_timeout=1000")
				if err != nil {
					Error(http.StatusInternalServerError, w, err)
					return
				}
				defer dbh.Close()
				utbl := users.New()
				tx, err := dbh.Begin()
				if err != nil  {
					Error(http.StatusInternalServerError, w, err)
					return
				}
				defer tx.Rollback()
				m_users, err := utbl.MatchLogin(tx, s.Username, s.Password)
				if err != nil {
					Error(http.StatusInternalServerError, w, err)
					return
				}
				if len(m_users) == 0 {
					// no users found
					Error(http.StatusUnauthorized, w, errors.New("Incorrect username/password"))
					return
				} else if len(m_users) == 1 {
					// user found
					// create a session
					session, err := sessions.GetSessionManager().CreateSession(s.Username)
					if err != nil {
						Error(http.StatusInternalServerError, w, err)
						return
					}
					sess := SessionResponseData{session.SessionId}
					Success(w, sess)
				}
			}
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		r.Close = true
	}
}

func SessionInit() error {
	http.HandleFunc("/rasta/api/session/", session_api_handler)
	return nil
}

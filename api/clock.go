package api

import (
	"net/http"
	"errors"
	"bitbucket.org/dkninja/rasta/sessions"
	"bitbucket.org/dkninja/rasta/db"
	"bitbucket.org/dkninja/rasta/schema/clocks"
	"bitbucket.org/dkninja/rasta/schema/users"
	"time"
	"encoding/json"
)

func clock_api_handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		if !UserLoggedIn(r) {
			Error(http.StatusUnauthorized, w, errors.New("User has not authenticated yet"))
			return
		}
		c, err := r.Cookie("session_id")
		if err != nil {
			// error fetching cookie
			Error(http.StatusBadRequest, w, err)
		}
		// cookie found
		// lookup session id in active map
		sessions.GetSessionManager().SessionLock.Lock()
		defer sessions.GetSessionManager().SessionLock.Unlock()
		session := sessions.GetSessionManager().Sessions[c.Value]
		if session == nil {
			Error(http.StatusUnauthorized, w, errors.New("Unable to find session"))
		}

		// Now let's go and find the login status
		dbh, err := db.OpenHelper("sqlite3", "./rasta.db?_busy_timeout=1000")
		if err != nil {
			panic(err)
		}
		defer dbh.Close()
		ctbl := clocks.New()
		utbl := users.New()
		tx, err := dbh.Begin()
		if err != nil  {
			panic(err)
		}
		defer tx.Rollback()

		db.Lock()
		defer db.Unlock()

		clocked_in, err := ctbl.IsUserClockedIn(tx, session.Username)
		if err != nil {
			panic(err)
		}

		if clocked_in {
			return
		}

		ur, err := utbl.GetUserByUsername(tx, session.Username)
		if err != nil {
			panic(err)
		}

		if len(ur) != 1 {
			panic(errors.New("Too many records found"))
		}

		ti := clocks.InitRecord(ur[0].GetUserid(), time.Now())
		_, err = ctbl.InsertRow(tx, ti)
		if err != nil {
			panic(err)
		}

		tx.Commit()

		Success(w, nil)

		r.Close = true
	case http.MethodPatch:
		if !UserLoggedIn(r) {
			// go to login page
			http.Redirect(w, r, "/rasta/login/", http.StatusSeeOther)
			return
		}
		c, err := r.Cookie("session_id")
		if err != nil {
			// error fetching cookie
			panic(err)
		}
		// cookie found
		// lookup session id in active map
		sessions.GetSessionManager().SessionLock.Lock()
		defer sessions.GetSessionManager().SessionLock.Unlock()
		session := sessions.GetSessionManager().Sessions[c.Value]
		if session == nil {
			panic(errors.New("Unable to find session"))
		}

		// Now let's go and find the login status
		dbh, err := db.OpenHelper("sqlite3", "./rasta.db?_busy_timeout=1000")
		if err != nil {
			panic(err)
		}
		defer dbh.Close()
		ctbl := clocks.New()
		tx, err := dbh.Begin()
		if err != nil  {
			panic(err)
		}
		defer tx.Rollback()

		db.Lock()
		defer db.Unlock()

		clocked_in, err := ctbl.IsUserClockedIn(tx, session.Username)
		if err != nil {
			panic(err)
		}

		if !clocked_in {
			return
		}

		err = ctbl.ClockOutUser(tx, time.Now(), session.Username)
		if err != nil {
			panic(err)
		}

		tx.Commit()

		encoder := json.NewEncoder(w)
		//encoder.SetEscapeHTML(true)
		resp := ResponseType{true, []string{}, nil}
		err = encoder.Encode(resp)
		if err != nil {
			panic(err)
		}

		r.Close = true
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		r.Close = true
	}
}

func ClockInit() error {
	http.HandleFunc("/rasta/api/clock/", clock_api_handler)
	return nil
}


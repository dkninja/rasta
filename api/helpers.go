package api

import (
	"encoding/json"
	"net/http"
	"log"
	"bytes"
)

type ResponseType struct {
	Success bool `json:"success"`
	Errors []string `json:"errors"`
	Data interface{} `json:"data,omitempty"`
}

func Error(code int, w http.ResponseWriter, myErr error) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	encoder := json.NewEncoder(w)
	//encoder.SetEscapeHTML(true)
	errors := make([]string, 0)
	errors = append(errors, myErr.Error())
	resp := ResponseType{false, errors, nil}
	err := encoder.Encode(resp)
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}

func Success(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(w)
	//encoder.SetEscapeHTML(true)
	errors := make([]string, 0)
	resp := ResponseType{true, errors, data}
	err := encoder.Encode(resp)
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}

func EncodeRequest(req interface{}) (bytes.Buffer, error) {
	var api_buf bytes.Buffer
	encoder := json.NewEncoder(&api_buf)
	err := encoder.Encode(req)
	return api_buf, err
}

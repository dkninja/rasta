package sessions

import (
	"time"
	"encoding/hex"
	"crypto/rand"
	"sync"
)

type Session struct {
	SessionId string
	Username string
	SessionStart time.Time
	ExpiryTime time.Time
}

func generateSessionId() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	session_id := hex.EncodeToString(b)
	return session_id, err
}

func NewSession(username string) (*Session, error) {
	id, err := generateSessionId()
	if err != nil {
		return nil, err
	}
	return &Session{id, username, time.Now(), time.Now().Add(time.Minute * 10)}, err
}

func (s *Session) IsExpired() bool {
	if s.ExpiryTime.Before(time.Now()) {
		// Expiry time is before now
		return true
	}
	return false
}

type SessionManager struct {
	Sessions map[string]*Session
	SessionLock sync.Mutex
	reaperTimer *time.Timer
}

var manager *SessionManager = nil

func InitSessionManager() {
	if manager == nil {
		manager = &SessionManager{Sessions: map[string]*Session{}}
	}
}

func GetSessionManager() *SessionManager {
	if manager == nil {
		InitSessionManager()
	}
	return manager
}

func SessionReaper() {
	manager.SessionLock.Lock()
	defer manager.SessionLock.Unlock()
	for k, v := range manager.Sessions {
		if v.IsExpired() {
			delete(manager.Sessions, k)
		}
	}
	manager.reaperTimer = time.AfterFunc(time.Minute, SessionReaper)
}

func (s *SessionManager) CreateSession(username string) (*Session, error) {
	session, err := NewSession(username)
	if err != nil {
		return nil, err
	}
	manager.SessionLock.Lock()
	defer manager.SessionLock.Unlock()
	s.Sessions[session.SessionId] = session
	return session, err
}

func (s *SessionManager) StartSessionReaper() {
	manager.SessionLock.Lock()
	defer manager.SessionLock.Unlock()
	manager.reaperTimer = time.AfterFunc(time.Minute, SessionReaper)
}


func FiniSessionManager() {
	if manager != nil {
		for stop := manager.reaperTimer.Stop(); !stop; {
			// timer has already expired, try again
		}
	}
	manager = nil
}

package main

import (
	"net/http"
	"log"
	"os"
	"flag"
	"strconv"
	"bitbucket.org/dkninja/rasta/sessions"
	"bitbucket.org/dkninja/rasta/db"
	"bitbucket.org/dkninja/rasta/api"
	"bitbucket.org/dkninja/rasta/ui"
	"bitbucket.org/dkninja/rasta/schema/users"
	"bitbucket.org/dkninja/rasta/schema/clocks"
)

func main() {
	var port = flag.Int("port", 80, "port to listen on")

	sessions.InitSessionManager()
	sessions.GetSessionManager().StartSessionReaper()

	api.Init()

	ui.HomeInit()
	ui.LoginInit()
	ui.UsersInit()
	//ui.UsersAddInit()

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))

	// init the database if it doesn't exist
	if _, err := os.Open("./rasta.db"); os.IsNotExist(err) {
		dbh, err := db.OpenHelper("sqlite3", "./rasta.db?_busy_timeout=1000")
		if err != nil {
			log.Panicf("Unable to create database %s", err)
		}
		defer dbh.Close()
		utbl := users.New()
		ctbl := clocks.New()
		tx, err := dbh.Begin()
		if err != nil  {
			log.Panicf("Unable to start transaction%s", err)
		}
		defer tx.Rollback()
		utbl.CreateTable(tx)
		ctbl.CreateTable(tx)
		tx.Commit()
	}

	flag.Parse()
	port_string := ":" + strconv.Itoa(*port)
	log.Println("Starting server")
	log.Fatal(http.ListenAndServe(port_string, nil))
	sessions.FiniSessionManager()
}

package clocks

import (
	"database/sql"
	"log"
	"errors"
	"time"
)

type Clocks struct {
	version int
}

func New() *Clocks{
	return &Clocks{1}
}

// Create a Clock table for the database
func (tbl *Clocks) CreateTable(tx *sql.Tx) error {
	if tbl.version == 1 {
		sqlStmt := `
		CREATE TABLE clock (
			record_id INTEGER PRIMARY KEY autoincrement,
			user_id INTEGER NOT NULL,
			time_in timestamp NOT NULL,
			time_out timestamp,
			FOREIGN KEY(user_id) REFERENCES users(user_id)
		)
		`
		_, err := tx.Exec(sqlStmt)
		if err != nil {
			log.Printf("%q: %s\n", err, sqlStmt)
			return err
		}
		return err
	} else {
		return errors.New("Wrong table version")
	}
}

func (tbl *Clocks) InsertRow(tx *sql.Tx, record *ClockRecord) (int64, error) {
	sqlStmt := `
	INSERT INTO clock (
		user_id, time_in, time_out)
		VALUES(?, ?, ?)
	`

	result, err := tx.Exec(sqlStmt, record.GetUserid(), record.GetTimeIn(), nil)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return 0, err
	}
	id, err := result.LastInsertId()
	if err == nil {
		//record.SetRecordid(id)
		record.record_id = id
	}
	return id, err
}

func (tbl *Clocks) DeleteRow(tx *sql.Tx, record *ClockRecord) (bool, error) {
	sqlStmt := `
	DELETE FROM clock WHERE record_id = ?
	`
	result, err := tx.Exec(sqlStmt, record.GetRecordid())
	if err != nil {
		return false, err
	}
	num, err := result.RowsAffected()
	if err == nil && num > 1 {
		return false, errors.New("Too many rows affected")
	}
	return num == 1, nil
}

func (tbl *Clocks) UpdateRow(tx *sql.Tx, id int64, record *ClockRecord) error {
	sqlStmt := `
	UPDATE clock SET user_id = ?, time_in = ?, time_out = ? WHERE record_id = ?
	`

	var result sql.Result
	var err error

	if record.GetTimeOut().IsZero() {
		result, err = tx.Exec(sqlStmt, record.GetUserid(), record.GetTimeIn(), nil,
			record.GetRecordid())
	} else {
		result, err = tx.Exec(sqlStmt, record.GetUserid(), record.GetTimeIn(),
			record.GetTimeOut(), record.GetRecordid())
	}

	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return err
	}
	c, err := result.RowsAffected()
	if err == nil && c != 1 {
		log.Fatal("Wrong number of rows changed", c)
		return errors.New("Wrong number of rows changed")
	}
	return err
}

func populate_clock_records(rows *sql.Rows) ([]ClockRecord, error) {
	var recs []ClockRecord = make([]ClockRecord, 0)
	for rows.Next() {
		var record_id, user_id int64
		var time_in time.Time
		var time_out *time.Time

		err := rows.Scan(&record_id, &user_id, &time_in, &time_out)
		if err != nil {
			return recs, err
		}

		if time_out != nil {
			recs = append(recs, ClockRecord{record_id, user_id, time_in, *time_out})
		} else {
			recs = append(recs, ClockRecord{record_id, user_id, time_in, time.Time{}})
		}
	}
	err := rows.Err()
	return recs, err
}

func (tbl *Clocks) GetRecordByRecordid(tx *sql.Tx, id int64) ([]ClockRecord, error) {
	sqlStmt := `
	SELECT * FROM clock WHERE record_id=?
	`
	rows, err := tx.Query(sqlStmt, id)
	if err != nil {
		return make([]ClockRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_clock_records(rows)
	return recs, err
}

func (tbl *Clocks) IsUserClockedIn(tx *sql.Tx, username string) (bool, error) {
	sqlStmt := `
	SELECT record_id FROM clock
	INNER JOIN users ON clock.user_id = users.user_id
	WHERE users.username=?
	AND time_out IS NULL
	`
	var record_id int64
	err := tx.QueryRow(sqlStmt, username).Scan(&record_id)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		} else {
			return false, err
		}
	}

	return true, nil
}

func (tbl *Clocks) ClockOutUser(tx *sql.Tx, t time.Time, username string) error {
	sqlStmt := `
	UPDATE clock
	SET time_out = ?
	WHERE time_out is NULL AND
	user_id IN (SELECT user_id FROM users WHERE username = ?)
	`

	var result sql.Result
	var err error

	result, err = tx.Exec(sqlStmt, t, username)

	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return err
	}
	c, err := result.RowsAffected()
	if err == nil && c != 1 {
		log.Fatal("Wrong number of rows changed", c)
		return errors.New("Wrong number of rows changed")
	}
	return err
}

func (tbl *Clocks) GetUserHistory(tx *sql.Tx, username string, count int64) ([]ClockRecord, error) {
	sqlStmt := `
	SELECT clock.* FROM clock
	INNER JOIN users on clock.user_id = users.user_id
	WHERE users.username=?
	ORDER BY clock.record_id DESC LIMIT ?
	`
	rows, err := tx.Query(sqlStmt, username, count)
	if err != nil {
		return make([]ClockRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_clock_records(rows)
	return recs, err
}

type ClockRecord struct {
	record_id int64
	user_id int64
	time_in time.Time
	time_out time.Time
}

func NewRecord() *ClockRecord {
	return &ClockRecord{}
}

func InitRecord(userid int64, timein time.Time) *ClockRecord {
	return &ClockRecord{user_id: userid, time_in: timein}
}

func (c *ClockRecord) GetRecordid() int64 {
	return c.record_id
}

func (c *ClockRecord) GetUserid() int64 {
	return c.user_id
}

func (c *ClockRecord) GetTimeIn() time.Time {
	return c.time_in
}

func (c *ClockRecord) GetTimeOut() time.Time {
	return c.time_out
}

func (c *ClockRecord) SetTimeOut(t time.Time) {
	c.time_out = t
}


package users

import (
	"database/sql"
	"log"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"bytes"
)

type Users struct {
	version int
}

func New() *Users {
	return &Users{1}
}

func _StringToByte(s string) []byte {
	buf := bytes.NewBufferString(s)
	return buf.Bytes()
}

func _BytesToString(b []byte) string {
	buf := bytes.NewBuffer(b)
	return buf.String()
}

func HashPassword(s string) (string, error) {
	b, err := bcrypt.GenerateFromPassword(_StringToByte(s), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return _BytesToString(b), err
}

// Create a User table for the database
func (tbl *Users) CreateTable(tx *sql.Tx) error {
	if tbl.version == 1 {
		sqlStmt := `
		CREATE TABLE users (
			user_id INTEGER PRIMARY KEY autoincrement,
			username varchar(50) UNIQUE NOT NULL,
			firstname text NOT NULL,
			lastname text NOT NULL,
			password text NOT NULL,
			admin INTEGER NOT NULL,
			disabled INTEGER NOT NULL
		)
		`
		_, err := tx.Exec(sqlStmt)
		if err != nil {
			log.Printf("%q: %s\n", err, sqlStmt)
			return err
		}
		password , err := HashPassword("admin")
		if err != nil {
			log.Printf("%q\n", err)
			return err
		}
		var ur *UserRecord = InitRecord("admin", "admin", "admin", password, true, false)
		_, err = tbl.InsertRow(tx, ur)
		return err
	} else {
		return errors.New("Wrong table version")
	}
}

func (tbl *Users) InsertRow(tx *sql.Tx, record *UserRecord) (int64, error) {
	sqlStmt := `
	INSERT INTO users (
		username, firstname, lastname, password, admin, disabled)
		VALUES(?, ?, ?, ?, ?, ?)
	`

	result, err := tx.Exec(sqlStmt, record.GetUsername(), record.GetFirstname(),
		record.GetLastname(), record.GetPassword(), record.GetAdmin(), record.GetDisabled())
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return 0, err
	}
	id, err := result.LastInsertId()
	if err == nil {
		record.user_id = id
	}
	return id, err
}

func (tbl *Users) DeleteRow(tx *sql.Tx, id int64) (bool, error) {
	sqlStmt := `
	DELETE FROM users WHERE user_id = ?
	`
	result, err := tx.Exec(sqlStmt, id)
	if err != nil {
		return false, err
	}
	num, err := result.RowsAffected()
	if err == nil && num > 1 {
		return false, errors.New("Too many rows affected")
	}
	return num == 1, nil
}

func (tbl *Users) UpdateRow(tx *sql.Tx, id int64, record *UserRecord) error {
	sqlStmt := `
	UPDATE users SET username = ?, firstname = ?, lastname = ?, password = ?, admin = ?,
	disabled = ? WHERE user_id = ?)
	`

	result, err := tx.Exec(sqlStmt, record.GetUsername(), record.GetFirstname(),
		record.GetLastname(), record.GetPassword(), record.GetAdmin(), record.GetDisabled(),
		record.GetUserid())
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return err
	}
	c, err := result.RowsAffected()
	if err == nil && c == 1 {
		log.Fatal("Wrong number of rows changed", c)
		return errors.New("Wrong number of rows changed")
	}
	return err
}

func populate_user_records(rows *sql.Rows) ([]UserRecord, error) {
	var recs []UserRecord = make([]UserRecord, 0)
	for rows.Next() {
		var id int64
		var username, firstname, lastname string
		var password string
		var admin, disabled bool

		err := rows.Scan(&id, &username, &firstname, &lastname, &password, &admin, &disabled)
		if err != nil {
			return recs, err
		}
		recs = append(recs, UserRecord{id, username, firstname, lastname, password, admin, disabled})
	}
	err := rows.Err()
	return recs, err
}

func (tbl *Users) GetActiveUsers(tx *sql.Tx) ([]UserRecord, error) {
	sqlStmt := `
	SELECT * FROM users WHERE disabled = ?
	`
	rows, err := tx.Query(sqlStmt, false)
	if err != nil {
		return make([]UserRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_user_records(rows)
	return recs, err
}

func (tbl *Users) GetInactiveUsers(tx *sql.Tx) ([]UserRecord, error) {
	sqlStmt := `
	SELECT * FROM users WHERE disabled = ?
	`
	rows, err := tx.Query(sqlStmt, true)
	if err != nil {
		return make([]UserRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_user_records(rows)
	return recs, err
}

func (tbl *Users) GetUserByUserid(tx *sql.Tx, user_id int64) ([]UserRecord, error) {
	sqlStmt := `
	SELECT * FROM users WHERE user_id= ?
	`
	rows, err := tx.Query(sqlStmt, user_id)
	if err != nil {
		return make([]UserRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_user_records(rows)
	return recs, err
}

func (tbl *Users) GetUserByUsername(tx *sql.Tx, username string) ([]UserRecord, error) {
	sqlStmt := `
	SELECT * FROM users WHERE username= ?
	`
	rows, err := tx.Query(sqlStmt, username)
	if err != nil {
		return make([]UserRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_user_records(rows)
	return recs, err
}

func (tbl *Users) MatchLogin(tx *sql.Tx, username string, password string) ([]UserRecord, error) {
	sqlStmt := `
	SELECT * FROM users WHERE disabled = ? AND username = ?
	`
	rows, err := tx.Query(sqlStmt, false, username)
	if err != nil {
		return make([]UserRecord, 0), err
	}
	defer rows.Close()

	recs, err := populate_user_records(rows)
	if len(recs) > 1 {
		return make([]UserRecord, 0), errors.New("Too many records found")
	} else if len(recs) == 1 {
		err = bcrypt.CompareHashAndPassword(_StringToByte(recs[0].password), _StringToByte(password))
		if err != nil {
			return make([]UserRecord, 0), nil
		} else {
			// same password
		}
	} else {
		return make([]UserRecord, 0), nil
	}
	return recs, err
}

type UserRecord struct {
	user_id int64
	username string
	firstname string
	lastname string
	password string
	admin bool
	disabled bool
}

func NewRecord() *UserRecord {
	return &UserRecord{}
}

func InitRecord(username string, firstname string, lastname string,
			    password string, admin bool, disabled bool) *UserRecord {
	return &UserRecord{0, username, firstname, lastname, password, admin, disabled}
}

func (u *UserRecord) GetUserid() int64{
	return u.user_id
}

func (u *UserRecord) GetUsername() string {
	return u.username
}

func (u *UserRecord) GetFirstname() string {
	return u.firstname
}

func (u *UserRecord) GetLastname() string {
	return u.lastname
}

func (u *UserRecord) GetPassword() string {
	return u.password
}

func (u *UserRecord) GetAdmin() bool {
	return u.admin
}

func (u *UserRecord) GetDisabled() bool {
	return u.disabled
}

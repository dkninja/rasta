package main

import (
	"bitbucket.org/dkninja/rasta/schema/users"
	"bitbucket.org/dkninja/rasta/schema/clocks"
	db2 "bitbucket.org/dkninja/rasta/db"
	"os"
	"time"
	"testing"
)

func TestSchemaOps(t *testing.T) {
	defer os.Remove("./foo.db")

	db, err := db2.OpenHelper("sqlite3", "./foo.db")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	utbl := users.New()
	ctbl := clocks.New()
	tx, err := db.Begin()
	if err != nil  {
		t.Fatal(err)
	}
	defer tx.Rollback()
	utbl.CreateTable(tx)
	ctbl.CreateTable(tx)
	tx.Commit()

	tx, err = db.Begin()

	password , _ := users.HashPassword("password")
	r := users.InitRecord("test", "firstname", "lastname", password, false, true)
	id, err := utbl.InsertRow(tx, r)
	if err != nil {
		t.Fatal(err)
	} else {
		if id != 2 {
			t.Fatal("Wrong ID assigned %d to record", id)
		}
		if r.GetUserid() != 2 {
			t.Fatal("Wrong ID assigned %d to record", r.GetUserid())
		}
	}
	password , _ = users.HashPassword("password")
	r = users.InitRecord("test2", "", "", password, false, false)
	id, err = utbl.InsertRow(tx, r)
	if err != nil {
		t.Fatal(err)
	}
	password , _ = users.HashPassword("password2")
	r = users.InitRecord("test3", "", "", password, false, false)
	id, err = utbl.InsertRow(tx, r)
	if err != nil {
		t.Fatal(err)
	}

	ti := clocks.InitRecord(id, time.Now())
	id, err = ctbl.InsertRow(tx, ti)
	if err != nil {
		t.Fatal(err)
	}
	tx.Commit()

	tx, err = db.Begin()
	clockrecs, err := ctbl.GetRecordByRecordid(tx, id)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(1)
	clockrecs[0].SetTimeOut(time.Now())
	ctbl.UpdateRow(tx, clockrecs[0].GetRecordid(), &clockrecs[0])
	tx.Commit()

	tx, err = db.Begin()
	userrecs, err := utbl.GetActiveUsers(tx)
	if err != nil {
		t.Fatal(err)
	}
	if len(userrecs) != 3 {
		t.Fatal(len(userrecs))
	}

	// find one admin user
	userrecs, err = utbl.MatchLogin(tx, "admin", "admin")
	if err != nil {
		t.Fatal(err)
	}
	if len(userrecs) != 1 {
		t.Fatal(len(userrecs))
	}

	// find 0 admin user with wrong password
	userrecs, err = utbl.MatchLogin(tx, "admin", "adm")
	if err != nil {
		t.Fatal(err)
	}
	if len(userrecs) != 0 {
		t.Fatal(len(userrecs))
	}

	// find test3 user
	userrecs, err = utbl.MatchLogin(tx, "test3", "password2")
	if err != nil {
		t.Fatal(err)
	}
	if len(userrecs) != 1 {
		t.Fatal(len(userrecs))
	}

	deleted, err := utbl.DeleteRow(tx, clockrecs[0].GetUserid())
	if deleted || err == nil {
		t.Fatal(deleted, err)
	}
	tx.Commit()
}

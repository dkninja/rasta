package db

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"sync"
)

var lock sync.Mutex

func OpenHelper(driverName string, dataSourceName string) (*sql.DB, error) {
	db, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}
	if driverName == "sqlite3" {
		db.Exec("PRAGMA foreign_keys = ON")
	}
	return db, err
}

func Lock() {
	lock.Lock()
}

func Unlock() {
	lock.Unlock()
}

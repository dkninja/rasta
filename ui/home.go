package ui

import (
	"net/http"
	"html/template"
	"errors"
	"time"
	"bitbucket.org/dkninja/rasta/sessions"
	"bitbucket.org/dkninja/rasta/db"
	"bitbucket.org/dkninja/rasta/api"
	"bitbucket.org/dkninja/rasta/schema/clocks"
	"bitbucket.org/dkninja/rasta/schema/users"
)

type HomeData struct {
	Firstname string
	Lastname string
	Username string
	ClockedIn bool
	Records []ClockRecordTpl
}

type ClockRecordTpl struct {
	TimeIn time.Time
	TimeOut time.Time
	TimedOut bool
}

func ConvertClockRecord(r clocks.ClockRecord) ClockRecordTpl {
	if r.GetTimeOut().IsZero() {
		return ClockRecordTpl{TimeIn: r.GetTimeIn(),
		                      TimeOut: r.GetTimeOut(),
		                      TimedOut: false}
	} else {
		return ClockRecordTpl{TimeIn: r.GetTimeIn(),
		                      TimeOut: r.GetTimeOut(),
		                      TimedOut: true}
	}
}

func FormattedDate(t time.Time) string {
	return t.Format("Jan 2 2006 15:04:05")
}

func home_page_handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		if !api.UserLoggedIn(r) {
			// go to login page
			http.Redirect(w, r, "/rasta/login/", http.StatusSeeOther)
			return
		}
		c, err := r.Cookie("session_id")
		if err != nil {
			// error fetching cookie
			panic(err)
		}
		// cookie found
		// lookup session id in active map
		sessions.GetSessionManager().SessionLock.Lock()
		defer sessions.GetSessionManager().SessionLock.Unlock()
		session := sessions.GetSessionManager().Sessions[c.Value]
		if session == nil {
			panic(errors.New("Unable to find session"))
		}

		// Now let's go and find the login status
		dbh, err := db.OpenHelper("sqlite3", "./rasta.db?_busy_timeout=1000")
		if err != nil {
			panic(err)
		}
		defer dbh.Close()
		ctbl := clocks.New()
		utbl := users.New()
		tx, err := dbh.Begin()
		if err != nil  {
			panic(err)
		}
		defer tx.Rollback()

		urec, err := utbl.GetUserByUsername(tx, session.Username)
		if err != nil {
			panic(err)
		}

		if len(urec) != 1 {
			panic(errors.New("Too many records returned"))
		}

		clocked_in, err := ctbl.IsUserClockedIn(tx, session.Username)
		if err != nil {
			panic(err)
		}

		records, err := ctbl.GetUserHistory(tx, session.Username, 2)
		if err != nil {
			panic(err)
		}

		// And print
		t := home_templates["home"]
		var records_tpl []ClockRecordTpl
		for _, r := range records {
			records_tpl = append(records_tpl, ConvertClockRecord(r))
		}
		d := HomeData{urec[0].GetFirstname(), urec[0].GetLastname(), session.Username, clocked_in, records_tpl}
		err = t.ExecuteTemplate(w, "home", d)
		if err != nil {
			panic(err)
		}
		r.Close = true
	default:
		w.WriteHeader(http.StatusBadRequest)
		r.Close = true
	}
}

var home_templates map[string]*template.Template

func HomeInit() error {
	funcMap := template.FuncMap{
		"ftime": FormattedDate,
	}

	home_templates = map[string]*template.Template {
		"home": template.Must(template.New("home").Funcs(funcMap).ParseFiles("ui-template/header.tpl", "ui-template/menu.tpl", "ui-template/home.tpl")),
	}
	http.HandleFunc("/rasta/", home_page_handler)
	return nil
}

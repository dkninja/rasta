package ui

import (
	"net/http"
	"html/template"
	"encoding/json"
	"bitbucket.org/dkninja/rasta/api"
)

type LoginData struct {
	Username string
	UserErr bool
	PassErr bool
}

func login_page_handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		t := login_templates["login"]
		d := LoginData{}
		err := t.ExecuteTemplate(w, "login", d)
		if err != nil {
			panic(err)
		}
		r.Close = true
	case http.MethodPost:
		err := r.ParseForm()
		if err != nil {
			panic(err)
		}
		api_req := api.SessionRequest{r.Form.Get("username"), r.Form.Get("password")}
		api_buf, err := api.EncodeRequest(api_req)
		if err != nil {
			panic(err)
		}
		url := "http://" + r.Host + "/rasta/api/session/"
		api_req_enc, err := http.NewRequest("POST", url, &api_buf)
		api_req_enc.Header.Set("Content-Type", "application/json")
		client := &http.Client{}
		api_resp, err := client.Do(api_req_enc)
		if err != nil {
			panic(err)
		}
		defer api_resp.Body.Close()
		if api_resp.StatusCode != http.StatusOK {
			// Reprint the form with old values
			t := login_templates["login"]
			d := LoginData{r.Form.Get("username"), false, true}
			err := t.ExecuteTemplate(w, "login", d)
			if err != nil {
				panic(err)
			}
		} else {
			dec := json.NewDecoder(api_resp.Body)
			type session_id_response_type struct {
				SessionId string `json:"session_id"`
			}
			type response_type struct {
				Success bool `json:"success"`
				Errors []string `json:"errors"`
				Data session_id_response_type `json:"data"`
			}

			var resp response_type
			err = dec.Decode(&resp);
			if err != nil {
				panic(err)
			}
			cookie := http.Cookie{Name: "session_id", Value: resp.Data.SessionId, Path: "/"}
			http.SetCookie(w, &cookie)
			// go to home page
			http.Redirect(w, r, "/rasta/", http.StatusSeeOther)
		}
	default:
		w.WriteHeader(http.StatusBadRequest)
	}
}

var login_templates map[string]*template.Template

func LoginInit() error {
	login_templates = map[string]*template.Template {
		"login": template.Must(template.ParseFiles("ui-template/header.tpl", "ui-template/login.tpl")),
	}
	http.HandleFunc("/rasta/login/", login_page_handler)
	return nil
}

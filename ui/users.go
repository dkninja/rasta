package ui

import (
	"net/http"
	"html/template"
	"errors"
	"bitbucket.org/dkninja/rasta/sessions"
	"bitbucket.org/dkninja/rasta/db"
	"bitbucket.org/dkninja/rasta/api"
	"bitbucket.org/dkninja/rasta/schema/users"
)

type UserPageData struct {
	IsAdmin bool
}

func users_page_handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		if !api.UserLoggedIn(r) {
			// go to login page
			http.Redirect(w, r, "/rasta/login/", http.StatusSeeOther)
			return
		}
		c, err := r.Cookie("session_id")
		if err != nil {
			// error fetching cookie
			panic(err)
		}
		// cookie found
		// lookup session id in active map
		sessions.GetSessionManager().SessionLock.Lock()
		defer sessions.GetSessionManager().SessionLock.Unlock()
		session := sessions.GetSessionManager().Sessions[c.Value]
		if session == nil {
			panic(errors.New("Unable to find session"))
		}

		// Now let's go and find the admin
		dbh, err := db.OpenHelper("sqlite3", "./rasta.db?_busy_timeout=1000")
		if err != nil {
			panic(err)
		}
		defer dbh.Close()
		utbl := users.New()
		tx, err := dbh.Begin()
		if err != nil  {
			panic(err)
		}
		defer tx.Rollback()

		urec, err := utbl.GetUserByUsername(tx, session.Username)
		if err != nil {
			panic(err)
		}

		if len(urec) != 1 {
			panic(errors.New("Too many records returned"))
		}


		// And print
		t := users_templates["users"]
		d := UserPageData{urec[0].GetAdmin()}
		err = t.ExecuteTemplate(w, "users", d)
		if err != nil {
			panic(err)
		}
		r.Close = true
	default:
		w.WriteHeader(http.StatusBadRequest)
		r.Close = true
	}
}

var users_templates map[string]*template.Template

func UsersInit() error {
	users_templates = map[string]*template.Template {
		"users": template.Must(template.ParseFiles("ui-template/header.tpl", "ui-template/menu.tpl", "ui-template/users.tpl")),
	}
	http.HandleFunc("/rasta/users/", users_page_handler)
	return nil
}
